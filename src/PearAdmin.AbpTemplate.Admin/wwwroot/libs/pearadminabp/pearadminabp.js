window.pearAdminAbpPath = (function (src) {
    src = document.currentScript
        ? document.currentScript.src
        : document.scripts[document.scripts.length - 1].src;
    return src.substring(0, src.lastIndexOf("/") + 1);
})();

function getPath(moduleName) {
    return '{/}' + pearAdminAbpPath + 'module/' + moduleName;
}

layui.extend({
    abp: getPath("abp"),
    abpcore: getPath("abpcore"),
    abpjquery: getPath("abpjquery"),
    abpnotice: getPath("abpnotice"),
    abpmessage: getPath("abpmessage"),
    abpsignalr: getPath("abpsignalr"),
    abpsignalrclient: getPath("abpsignalrclient"),
    abpsignalrchat: getPath("abpsignalrchat"),
    fullcalendar: getPath('fullcalendar'),
    social: getPath("social")
});